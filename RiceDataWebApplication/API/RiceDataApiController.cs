﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RiceDataWebApplication.Models;
using RiceDataWebApplication.Services;

namespace RiceDataWebApplication.API
{
    [Route("api/v1/rice")]
    [ApiController]
    public class RiceDataApiController : ControllerBase
    {
        private readonly RiceService _riceserviceMan;

        public RiceDataApiController(RiceService riceService)
        {
            this._riceserviceMan = riceService;
        }

        [HttpGet("all-rice",Name = "allRice")]
        public async Task<IActionResult> GetAllRiceAsync() 
        {
            var rices = await _riceserviceMan.GetAllRice();

            return Ok(rices);
        }

        [HttpPost("insert-rice",Name = "insertRice")]

        public async Task<ActionResult<ResponseModel>> InsertRiceAsync([FromBody] RiceModel value) 
        {
            var isSuccess = await _riceserviceMan.InsertRiceAsync(value);

            return Ok(new ResponseModel
            {
                ResponseMessage = "Success To Insert Rice"
            });
        }


        [HttpPut("update-rice",Name = "updateRice")]

        public async Task<ActionResult<ResponseModel>> UpdateRiceAsync([FromBody] RiceModel value) 
        {
            var isSuccess = await _riceserviceMan.UpdateRiceAsync(value);

            if (isSuccess == false) 
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Id Not Found!!!!"
                }) ;
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success update data {value.RiceName}"
            });
        }

        [HttpDelete("delete-rice", Name = "deleteRice")]

        public async Task<ActionResult<ResponseModel>> DeleteRiceAsync([FromBody] RiceModel value) 
        {
            var isSuccess = await _riceserviceMan.DeleteRiceAsync(value.RiceId);

            if (isSuccess == false) 
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Id Not Found!!!!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success Delete data {value.RiceName}"
            });
        }

        [HttpGet("specific-rice", Name = "specificRice")]

        public async Task<ActionResult<RiceModel>> getSpecificRiceAsync(int? riceId) 
        {
            if (riceId.HasValue == false) 
            {
                return BadRequest(null);
            }

            var rice = await _riceserviceMan.GetSpesificRiceAsync(riceId.Value);
            if (rice == null) 
            {
                return BadRequest(null);
            }
            return Ok(rice);
        }

        [HttpGet("filter-data")]
        public async Task<ActionResult<RiceModel>> GetFilterDataAsync([FromBody] int pageIndex,int itemPerPage,string filterByname)
        {
            var data = await _riceserviceMan.GetAsync(pageIndex,itemPerPage,filterByname);

            return Ok(data);
        }

        [HttpGet("total-data")]
        public ActionResult<int> getTotalData()
        {
            var totalData = _riceserviceMan.GetTotalData();

            return Ok(totalData);
        }

    }
}
