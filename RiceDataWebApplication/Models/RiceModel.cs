﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RiceDataWebApplication.Models
{
    public class RiceModel
    {
        public int RiceId { set; get; }

        [Required]
        [StringLength(7,MinimumLength =3)]
        [Display(Name = "Name")]
        public string RiceName { get; set; }

        [Required]
        [Display(Name = "Stock")]
        public int RiceStock { get; set; }
    }
}
