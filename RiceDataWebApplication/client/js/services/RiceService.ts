﻿import Axios from 'axios'
import { RiceModelTemp } from '../models/IRiceModel'
import { ResponseModelTemp } from '../models/ResponseModelTemp'

export class RiceService {
    async getAllRice(): Promise<RiceModelTemp[]> {
        const response = await Axios.get<RiceModelTemp[]>('/api/v1/rice/all-rice');
        if (response.status === 200) {
            return response.data;
        }
        return [];
    }

    async insertNewRice(data: RiceModelTemp): Promise<string> {
        const response = await Axios.post<ResponseModelTemp>('/api/v1/rice/insert-rice', data);
        if (response.status === 200) {
            return response.data.responseMessage
        }
        return response.data.responseMessage;
    }


}

export const RiceServiceSingleton = new RiceService();

