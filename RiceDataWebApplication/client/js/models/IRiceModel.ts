﻿export interface RiceModelTemp
{
    riceId?: string;
    name?: string;
    stock?: string;
}