﻿using Microsoft.EntityFrameworkCore;
using RiceData.Entities;
using RiceDataWebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiceDataWebApplication.Services
{
    public class RiceService
    {
        private readonly RiceDbContext _db;

        public RiceService(RiceDbContext dbContext)
        {
            this._db = dbContext;
        }

        public async Task<bool> InsertRiceAsync(RiceModel riceModel) 
        {
            this._db.Add(new Rice
            {
                RiceId = riceModel.RiceId,
                RiceName = riceModel.RiceName,
                RiceStock = riceModel.RiceStock
            });

            await this._db.SaveChangesAsync();
            return true;
        }

        public async Task<List<RiceModel>> GetAllRice() 
        {
            var rices = await this._db
                .Rices
                .Select(Q => new RiceModel
                {
                    RiceId = Q.RiceId,
                    RiceName = Q.RiceName,
                    RiceStock = Q.RiceStock
                })
                .AsNoTracking()
                .ToListAsync();

            return rices;
        }

        public async Task<bool> UpdateRiceAsync(RiceModel rice) 
        {
            var riceModel = await this._db
                .Rices
                .Where(Q => Q.RiceId == rice.RiceId)
                .FirstOrDefaultAsync();
            if (riceModel == null) 
            {
                return false;
            }

            riceModel.RiceName = rice.RiceName;
            riceModel.RiceStock = rice.RiceStock;

            await this._db.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteRiceAsync(int riceId) 
        {
            var riceModel = await this._db.Rices.Where(Q => Q.RiceId == riceId).FirstOrDefaultAsync();

            if (riceModel == null) 
            {
                return false;
            }

            this._db.Remove(riceModel);
            await this._db.SaveChangesAsync();

            return true;
        }

        public async Task<RiceModel> GetSpesificRiceAsync(int riceId) 
        {
            var rice = await this._db
                .Rices
                .Where(Q => Q.RiceId == riceId)
                .Select(Q => new RiceModel
                {
                    RiceId = Q.RiceId,
                    RiceName = Q.RiceName,
                    RiceStock = Q.RiceStock
                })
                .FirstOrDefaultAsync();

            return rice;
        }
        
        /// <summary>
        /// untuk mengambil total data
        /// </summary>
        /// <returns></returns>
        public int GetTotalData()
        {
            var totalRice = this._db
                .Rices
                .Count();

            return totalRice;
        }

        public async Task<List<RiceModel>> GetAsync(int pageIndex, int itemPerPage, string filterByName)
        {
            var query = this._db
                .Rices
                .AsQueryable();

            // for filter
            if (string .IsNullOrEmpty(filterByName) == false)
            {
                query = query
                    .Where(Q => Q.RiceName.StartsWith(filterByName));
            }

            var rices = await query
                .Select(Q => new RiceModel
                {
                    RiceId = Q.RiceId,
                    RiceName = Q.RiceName,
                    RiceStock = Q.RiceStock
                })
                .Skip((pageIndex - 1) * itemPerPage)
                .Take(itemPerPage)
                .AsNoTracking()
                .ToListAsync();

            return rices;
        }
    }
}
