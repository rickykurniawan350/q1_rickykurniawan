﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace RiceData.Entities
{
    public class RiceDbContextFactory : IDesignTimeDbContextFactory<RiceDbContext>
    {
        public RiceDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<RiceDbContext>();
            builder.UseSqlite("Data Source=reference.db");
            return new RiceDbContext(builder.Options);
        }

    }
}
