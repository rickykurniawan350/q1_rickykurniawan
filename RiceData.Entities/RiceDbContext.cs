﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace RiceData.Entities
{
    public class RiceDbContext : DbContext
    {
        public RiceDbContext(DbContextOptions<RiceDbContext> options) : base(options)
        {

        }

        public DbSet<Rice> Rices { set; get; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Rice>().ToTable("Rice");
        }
    }
}
