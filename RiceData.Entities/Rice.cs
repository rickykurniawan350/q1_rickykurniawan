﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RiceData.Entities
{
    public class Rice
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RiceId { get; set; }

        [Required]
        public string RiceName { get; set; }

        [Required]
        public int RiceStock { get; set; }
    }
}
